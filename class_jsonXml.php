<?php
/**
 * @author José Refugio Melecio Soto
 * @version 1.1 2019-07-06
 * @description Clase que recibe un JSON y genera un CFDi v3.3
 */

 class jsonXML{
    private $o_XML;
    private $o_comprobante;
    private $m_datos;
    private $m_impuestos;

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     *
     */
    function __construct()
    {
        $this->o_XML         = "";
        $this->o_comprobante = "";
        $this->m_datos       = array();
        $this->m_impuestos   = array();
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-10
     * @description funcion que recibe un json y retorna un xml
     * @param $e_JSON: json con la informacion que tendra el cfdi
     */
    public function jsonToXml($e_JSON)
    {
        $this->m_datos             = json_decode($e_JSON,true);
        $this->m_impuestos         = $this->m_datos['Impuestos'];
        $this->o_XML               = new DOMDocument("1.0","UTF-8");
        $this->o_XML->formatOutput = true;
        unset($this->m_datos['Impuestos']);
        $this->nodoComprobante();
        $this->nodoSimple('Emisor');
        $this->nodoSimple('Receptor');
        $this->nodoDetallesGeneral();
        $this->nodoImpuestos();
        return $this->xmlString();
    }


    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-14
     * @description funcion que recibe un json, buscar el certificado,genera el sello y retorna un xml
     * @param $e_JSON: json con la informacion que tendra el cfdi
     */
    public function jsonToXmlGet($e_JSON){
        $this->m_datos = json_decode($e_JSON,true);
        return $this->obtenerCertificado($this->m_datos['Emisor']['Rfc']);
    }

     /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Agregar el nodo comprobante
     */
    private function nodoComprobante()
    {
        $o_comprobante = $this->o_XML->createElementNS('http://www.sat.gob.mx/cfd/3', 'cfdi:Comprobante','');
        $o_atributo    = $this->o_XML->appendChild($o_comprobante);
        $o_atributo->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $o_atributo->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance' ,'xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');
        foreach($this->m_datos['Comprobante'] as $e_llave => $e_valor)
        {
            $o_atributo->setAttributeNS('' ,$e_llave, $e_valor);
        }
        unset($this->m_datos['Comprobante']);
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Agrega un nodo simple, como por ejemplo los nodos de receptor y emisor
     * @param $e_nodoSimple: Nodo a generar
     */
    private function nodoSimple($e_nodoSimple)
    {
        $o_emisor         = $this->o_XML->createElement("cfdi:{$e_nodoSimple}",'');
        $o_elementoEmisor = $this->o_XML->documentElement->appendChild($o_emisor);
        foreach($this->m_datos[$e_nodoSimple] as $e_llave => $e_valor)
        {
            $o_elementoEmisor->setAttribute($e_llave, $e_valor);
        }
        unset($this->m_datos[$e_nodoSimple]);
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Agrega el nodo conceptos, el objetivo es hacer una funcion que acepte datos para cualquier
     *              tipo de comprobantes.
     */
    private function nodoDetallesGeneral()
    {
        foreach($this->m_datos as $e_llave => $e_valor)
        {
            $o_elemento       = $this->o_XML->createElement("cfdi:{$e_llave}",'');
            $o_elementoAppend = $this->o_XML->documentElement->appendChild($o_elemento);
            if(is_array($e_valor))
            {
                foreach($e_valor as $e_llaveDetalle => $e_valorDetalle)
                {
                    $e_nombreElemento  = $this->singular($e_llave);
                    $o_elemento        = $this->o_XML->createElement("cfdi:{$e_nombreElemento}",'');
                    $o_elementoAppend2 = $o_elementoAppend->appendChild($o_elemento);
                    if(is_array($e_valorDetalle))
                    {
                        foreach($e_valorDetalle as $e_atributoLlave => $e_atributoValor)
                        {
                            if(is_array($e_atributoValor))
                            {
                                # VENTA: Aqui se agrega el nodo impuestos que está dentro del nodo Concepto
                                $e_nombreElemento  = $this->singular($e_atributoLlave);
                                $o_elemento        = $this->o_XML->createElement("cfdi:{$e_atributoLlave}",'');
                                $o_elementoAppend3 = $o_elementoAppend2->appendChild($o_elemento);
                                # VENTA: Aqui se itera para agregar los nodos hijos de Impuestos
                                foreach($e_atributoValor as $e_llaveSubDetalle => $e_valorSubDetalle)
                                {
                                    if(is_array($e_valorSubDetalle))
                                    {
                                        # VENTA: Se agrega el nodo Trasladados, hijo de Impuestos.
                                        $o_elemento        = $this->o_XML->createElement("cfdi:{$e_llaveSubDetalle}",'');
                                        $o_elementoAppend4 = $o_elementoAppend3->appendChild($o_elemento);

                                        $e_nombreElemento  = $this->singular($e_llaveSubDetalle);
                                        $o_elemento        = $this->o_XML->createElement("cfdi:{$e_nombreElemento}",'');
                                        $o_elementoAppend5 = $o_elementoAppend3->appendChild($o_elemento);
                                        foreach($e_valorSubDetalle as $e_llaveNivel4 => $e_valorNivel4)
                                        {
                                            if(is_array($e_valorNivel4))
                                            {
                                                echo "dentro<br>";
                                                # VENTA: Se agrega el nodo Trasladado hijo de trasladados
                                                $e_nombreElemento  = $this->singular($e_llaveSubDetalle);
                                                $o_elemento        = $this->o_XML->createElement("cfdi:{$e_nombreElemento}",'');
                                                $o_elementoAppend5 = $o_elementoAppend4->appendChild($o_elemento);
                                                foreach($e_valorNivel4 as $e_llaveNivel5 => $e_valorNivel5)
                                                {
                                                    if(is_array($e_valorNivel5))
                                                    {

                                                    }
                                                    else
                                                    {
                                                        # VENTA: Aqui se agrega los atributos del nodo Trasladados
                                                        $o_elementoAppend5->setAttribute($e_llaveNivel5, $e_valorNivel5);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                $o_elementoAppend5 = $o_elementoAppend4->appendChild($o_elemento);
                                                $o_elementoAppend5->setAttribute($e_llaveNivel4, $e_valorNivel4);
                                            }
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                # VENTA: Aqui se agregan los atributos a los nodos Concepto
                                $o_elementoAppend2->setAttribute($e_atributoLlave, $e_atributoValor);
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
        }
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Agrega el nodo Impuestos
     *
     */
    private function nodoImpuestos()
    {
        $o_impuestos         = $this->o_XML->createElement("cfdi:Impuestos",'');
        $o_elementoImpuestos = $this->o_XML->documentElement->appendChild($o_impuestos);
        foreach($this->m_impuestos as $e_llave => $e_valor)
        {
            if(is_array($e_valor))
            {
                $o_elementoNivel1        = $this->o_XML->createElement("cfdi:{$e_llave}",'');
                $o_elementoNivel1Append  = $o_elementoImpuestos->appendChild($o_elementoNivel1);
                $e_elementoSingura       = $this->singular($e_llave);
                $o_elementoNivel2        = $this->o_XML->createElement("cfdi:{$e_elementoSingura}",'');
                $o_elementoNivel2Append  = $o_elementoNivel1Append->appendChild($o_elementoNivel2);
                foreach($e_valor as $e_llaveAtributo => $e_valorAtrubuto){
                    $o_elementoNivel2Append->setAttribute($e_llaveAtributo, $e_valorAtrubuto);
                }
            }
            else
            {
                $o_elementoImpuestos->setAttribute($e_llave, $e_valor);
            }
        }
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Le quita la ultima letra a una palabra, ejemplo: Impuestos -> Impuesto
     *
     */
    private function singular($e_plural)
    {
        return substr($e_plural,0,strlen($e_plural)-1);
    }

    /**
     * @author José Refugio Melecio Soto
     * @version 1.1 2019-07-06
     * @description Retorna un string con el XML generado
     *
     */
    private function xmlString()
    {
       unset($this->m_datos);
       unset($this->m_impuestos);
       return $e_XML = $this->o_XML->saveXML();
    }

    private function cadena_certificados_limpia($e_STRING)
    {
      /*
      $e_STRING_CERTIFICADO = file_get_contents($e_STRING);
      $this->CSD            = openssl_x509_parse( openssl_x509_read( $e_STRING_CERTIFICADO ) );
      */
      preg_match('/-----BEGIN .+-----(.+)-----END .+-----/msi', $e_STRING, $m_MATCHES);
      $e_TMP = $m_MATCHES[1];
      $e_TMP = preg_replace('/\n/', '', $e_TMP);
      $e_TMP = preg_replace('/\r/', '', $e_TMP);
      return $e_TMP;
    }


    private function obtener_cadena_original_cfdi( $e_CADENA_XML )
    {
      $o_XML = new DOMDocument();
      $o_XML->loadXML(utf8_encode($e_CADENA_XML));
      # Extrae cadena original
      $o_XSLT = new XSLTProcessor();
      $o_XSL  = new DOMDocument();
      $o_XSL->load("cadenaOriginal/cadenaoriginal_3_3.xslt", LIBXML_NOCDATA);
      error_reporting( 0 ); # Se deshabilitan los errores pues el xssl de la cadena esta en version 2 y eso genera algunos warnings
      $o_XSLT->importStylesheet( $o_XSL );
      $o_COMPROBANTE = $o_XML->getElementsByTagName( 'Comprobante' )->item( 0 );
      return $o_XSLT->transformToXML($o_COMPROBANTE);
    }

    private function obtenerCertificado($e_RFC){
        # Consulta a la base de datos
        $e_host      = "site.stx.com.mx";
        $e_port      = "3706";
        $e_database  = "klay_delfeno_cfdi";
        $e_username  = "desarrollo";
        $e_password  = "VY3Q8qKF";
        $o_conection = new mysqli($e_host, $e_username, $e_password, $e_database,$e_port);
        if($o_conection->connect_errno) {
            echo "Falló la conexión a MySQL: (" . $o_conection->connect_errno . ") " . $conn->connect_error;
        }

        $e_query = "SELECT EMI_CERTIFICADO_CER,EMI_CERTIFICADO_KEY,EMI_KEYPASS FROM cat_emisores WHERE EMI_RFC = '{$e_RFC}'";
        if($r_resultado = $o_conection->query($e_query)){
            while($a_resultado = $r_resultado->fetch_array(MYSQLI_ASSOC)){
                $e_contrasena           = $a_resultado['EMI_KEYPASS'];
                $e_contenidoCertificado = $a_resultado['EMI_CERTIFICADO_CER'];
                #$e_CERT_KEY             = $a_resultado['EMI_CERTIFICADO_KEY'];
            }
        }
        else{
            echo "Falló la consulta";
        }

        #$e_contrasena           = "12345678a";
        $e_certificado          = "30001000000300023708";
        #$e_contenidoCertificado = file_get_contents("certificados/{$e_certificado}.cer.pem");
        $e_certificadoLimpio    = $this->cadena_certificados_limpia($e_contenidoCertificado);
        $e_XML                  = $this->jsonToXml(json_encode($this->m_datos));
        $e_cadenaOriginal       = $this->obtener_cadena_original_cfdi($e_XML);

        # Agregar Certificado
        $this->m_datos['Comprobante']['NoCertificado'] = $e_certificado;
        $this->m_datos['Comprobante']['Certificado']   = $e_certificadoLimpio;
        $this->m_datos['Comprobante']['Sello']         = "";
        $e_CERT_KEY   = file_get_contents("certificados/{$e_RFC}.key.pem");
        $e_KEY_LIMPIA = $this->cadena_certificados_limpia($e_CERT_KEY);
        # echo $e_KEY_LIMPIA; exit();
        $r_KEY        = openssl_pkey_get_private( $e_CERT_KEY );
        $e_CRYPTTEXT  = '';
        openssl_sign( $e_cadenaOriginal, $e_CRYPTTEXT, $r_KEY, 'SHA256' );

        # Cadena con el sello
        $e_SELLO     = base64_encode($e_CRYPTTEXT);

        # Se agrega el sello al XML
        $o_DOM = new DOMDocument();
        $o_DOM->formatOutput = true;
        $o_DOM->loadXML($e_XML);
        $o_COMPROBANTE = $o_DOM->getElementsByTagName('Comprobante')->item(0);
        $o_COMPROBANTE->setAttribute('Sello', $e_SELLO);

        return $e_CADENA_XML = $o_DOM->saveXML();
    }

 }
?>
