<?php
/**
 * @author José Refugio Melecio Soto
 * @version 1.0 2019-07-03
 * @description Fichero principal que llama a la clase classJsonToCfdi y le envia un JSON para
 *              generar un xml cfdi v3.3.
 */

  require_once("class_jsonXml.php");
  #json con sello
  #$e_archivo = file_get_contents("json.txt");

  #json sin sello
  $e_archivo = file_get_contents("jsonSinSello.txt");
  $xml = new jsonXML();
  #json con sello
  #$e_retornado = $xml->jsonToXML($e_archivo);

  #json sin sello
  $e_retornado = $xml->jsonToXMLGet($e_archivo);
  file_put_contents("filexml.xml",$e_retornado);
?>